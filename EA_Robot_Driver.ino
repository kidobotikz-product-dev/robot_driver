#include <ArduinoJson.h>
#include <Servo.h>
#include <limits.h>
//Navigation data constants
//left motor -> steer, right motor -> drive
const String DIRECTION_LEFT = "LEFT";
const String DIRECTION_RIGHT = "RIGHT";
const String DIRECTION_FORWARD = "FORWARD";
const String DIRECTION_STOP = "STOP";
const String DIRECTION_TURN_BACK = "TURN_BACK";
const String DIRECTION_ADJUST_LEFT = "LEFT";
const String DIRECTION_ADJUST_RIGHT = "RIGHT";
const String DIRECTION_LEFT_TOWARDS_OBSTACLE = "LEFT_TOWARDS_OBSTACLE";
const String DIRECTION_RIGHT_TOWARDS_OBSTACLE = "RIGHT_TOWARDS_OBSTACLE";
const String JSON_DATA_TYPE_DIRECTION = "DIRECTION";
const String JSON_DATA_TYPE_OBSTACLE = "OBSTACLE";

String navigationDataJson = "";


//Motor driver constants
const int DRIVE_OUTPUT_SIGNAL_PIN = 10;
const int STEER_OUTPUT_SIGNAL_PIN = 11;

const int SERVO_NEUTRAL = 1500;
//const int MAX_DRIVE_SPEED_OFFSET = 165;
//const int MAX_STEER_SPEED_OFFSET = 165;
//const int DRIVE_SERVO_NEUTRAL = 1500;
//const int STEER_SERVO_NEUTRAL = 1502;

const int MAX_DRIVE_SPEED_OFFSET = 140;
const int MAX_STEER_SPEED_OFFSET = 135;
const int MAX_STEER_TOWARDS_OBSTACLE_SPEED_OFFSET = 115;
int DRIVE_SERVO_NEUTRAL = 1500;
int STEER_SERVO_NEUTRAL = 1500;

const int DRIVE_SERVO_MAX = (DRIVE_SERVO_NEUTRAL + MAX_DRIVE_SPEED_OFFSET);
const int DRIVE_SERVO_MIN = (DRIVE_SERVO_NEUTRAL - MAX_DRIVE_SPEED_OFFSET);

const int STEER_SERVO_MAX = (STEER_SERVO_NEUTRAL + MAX_STEER_SPEED_OFFSET);
const int STEER_SERVO_MIN = (STEER_SERVO_NEUTRAL - MAX_STEER_SPEED_OFFSET);

const int STEER_TOWARDS_OBSTACLE_SERVO_MAX = (STEER_SERVO_NEUTRAL + MAX_STEER_TOWARDS_OBSTACLE_SPEED_OFFSET);
const int STEER_TOWARDS_OBSTACLE_SERVO_MIN = (STEER_SERVO_NEUTRAL - MAX_STEER_TOWARDS_OBSTACLE_SPEED_OFFSET);

const int MAX_CONNECTION_IDLE_PERIOD = 5000;

const int DEFAULT_SIMPLE_TURN_DURATION = 1300;
int simpleTurnDuration = DEFAULT_SIMPLE_TURN_DURATION;
const int DEFAULT_TURN_BACK_DURATION = 6000;
int turnBackDuration = DEFAULT_TURN_BACK_DURATION;
const int EYE_PART1_OUTPUT_PIN = 3;
const int EYE_PART2_OUTPUT_PIN = 5;


unsigned long lastBlinkedTimeMillis = 0;
boolean isEyeOpen = false;

Servo driveServo;
Servo steerServo;

boolean obstacleDetected = false;
unsigned long lastDataReceivedMillis;

boolean turningRight = false;
boolean turningLeft = false;
boolean turningBack = false;
unsigned long turnStartMillis;
unsigned long count;
boolean motorsStopped = false;
String navigationData = "";
boolean turnCompleted = false;
unsigned long previousSequenceNumber = -1;

bool adjustNeutralValue = 1;

void setup()
{

  /* add setup code here */
  Serial.begin(115200);
  pinMode(EYE_PART1_OUTPUT_PIN, OUTPUT);
  pinMode(EYE_PART2_OUTPUT_PIN, OUTPUT);
  driveServo.attach(DRIVE_OUTPUT_SIGNAL_PIN, 1000, 2000);
  steerServo.attach(STEER_OUTPUT_SIGNAL_PIN, 1000, 2000);
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_SERVO_NEUTRAL);


}

void loop()
{
  /* add main program code here */

  while (Serial.available()) {
    char readCharacter = Serial.read();
    if (readCharacter != '#') {
      navigationDataJson += readCharacter;
    }
    else {
//      Serial.print("json string: ");
//      Serial.println(navigationDataJson);
      
      navigationData = navigationDataJson;
      handleJson(navigationDataJson);
      navigationDataJson = "";
    }
  }
//  For manual commands through serial monitor
//  if (navigationData != "") {
//    handleJson(navigationData);
//  }
 
  if (count == ULONG_MAX) {
    count = 1;
  }
  monitorConnection();
  blinkEye();
}
void handleJson(String json) {
  //Serial.print("Received: ");
  //Serial.println(json);
  //json = json.replace(' ', '');
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& jsonObject = jsonBuffer.parseObject(json);
  if (!jsonObject.success()) {
    //Serial.println("parseObject() failed");
    return;
  }
  String jsonDataType = jsonObject["d_type"];
  String direction = jsonObject["direction"];
  int sequenceNumber = jsonObject["seq_num"];
  if (previousSequenceNumber != sequenceNumber) {
    lastDataReceivedMillis = millis();
  }
  previousSequenceNumber = sequenceNumber;
//  Serial.print("jsonDataType: ");
//  Serial.print(jsonDataType);
//  Serial.print(", direction: ");
//  Serial.println(direction);
  if (jsonDataType.equals(JSON_DATA_TYPE_DIRECTION)) {
    //Serial.println("Direction json");

    handleDirectionData(jsonObject["direction"]);

  }
  else {
    //Serial.println("not direction json");
  }
}

void handleDirectionData(String direction) {
  //Serial.println(direction);
  if (direction.equals(DIRECTION_LEFT)) {
//    Serial.println("Turn left");
    turnRobotLeft();
//        turnLeft();
//        printTurnStatusOngoing();
//        delay(DEFAULT_SIMPLE_TURN_DURATION);
//        stopMotors();
//        moveForward();
//        printTurnStatusCompleted();

  }
  else if (direction.equals(DIRECTION_RIGHT)) {
//        Serial.println("Turn right");
    turnRobotRight();
//        turnRight();
//        printTurnStatusOngoing();
//        delay(DEFAULT_SIMPLE_TURN_DURATION);
//        stopMotors();
//        moveForward();
//        printTurnStatusCompleted();

  }
  else if (direction.equals(DIRECTION_FORWARD)) {
//        Serial.println("move forward");
    simpleTurnDuration = DEFAULT_SIMPLE_TURN_DURATION;
    turnBackDuration = DEFAULT_TURN_BACK_DURATION;
    turningRight = false;
    turningLeft = false;
    turningBack = false;
    moveForward();
  }
  else if (direction.equals(DIRECTION_LEFT_TOWARDS_OBSTACLE)) {
    turnLeftTowardsObstacle();
  }
  else if (direction.equals(DIRECTION_RIGHT_TOWARDS_OBSTACLE)) {
    turnRightTowardsObstacle();
  }
  else if (direction.equals(DIRECTION_STOP)) {
    if (turningLeft || turningRight) {
      turningLeft = false;
      turningRight = false;
      printTurnStatusCompleted();
    }
    //calculateRemainingTurnTime();
    stopMotors();
  }
  else if (direction.equals(DIRECTION_TURN_BACK)) {
//    Serial.println("Turn back");
    turnRobotBack();
    
  }
  //Worst case
  else {
//    calculateRemainingTurnTime();
    if (turningLeft || turningRight) {
      turningLeft = false;
      turningRight = false;
      printTurnStatusCompleted();
    }
    stopMotors();
  }
  if (direction.equals(DIRECTION_STOP)) {
    motorsStopped = true;
  }
  else {
    motorsStopped = false;
  }
}

void handleObstacleData(bool isObstaclePresent) {
  if (isObstaclePresent) {
    Serial.println("Obstacle present");
    stopMotors();
    obstacleDetected = true;
  }
  else {
    Serial.println("No obstacle present");
    obstacleDetected = false;
  }
}


void turnRobotBack() {
  turnBack();
  if (!turningBack) {
    //Serial.println("turnRobotLeft");
    turnStartMillis = millis();
    printTurnStatusOngoing();
    turningBack = true;
  }
  else {
    //Serial.println("check if turn complete");
    if ((millis() - turnStartMillis) > turnBackDuration) {
      //Serial.println("turnCompleted");
      stopMotors();
      //moveForward();
      turningBack = false;
      printTurnStatusCompleted();
      turnBackDuration = DEFAULT_TURN_BACK_DURATION;
    }
  }
}
void turnRobotLeft() {
  turnLeft();
  if (!turningLeft) {
//    Serial.println("turnRobotLeft");
    turnStartMillis = millis();
    printTurnStatusOngoing();
   
    turningLeft = true;

  }
  else {
    //Serial.println("check if turn complete");
    if ((millis() - turnStartMillis) > simpleTurnDuration) {
      //Serial.println("turnCompleted");
      stopMotors();
      moveForward();
      turningLeft = false;
      turnCompleted = true;
      printTurnStatusCompleted();
      simpleTurnDuration = DEFAULT_SIMPLE_TURN_DURATION;
    }
  }
}
void turnRobotRight() {

  turnRight();
  if (!turningRight) {
    turnStartMillis = millis();
    printTurnStatusOngoing();
    turningRight = true;


  }
  else {
    if ((millis() - turnStartMillis) > simpleTurnDuration) {
      stopMotors();
      moveForward();
      turningRight = false;
      printTurnStatusCompleted();
      simpleTurnDuration = DEFAULT_SIMPLE_TURN_DURATION;
    }
  }
}


void turnRight() {
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_SERVO_MAX);
}

void turnLeft() {
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_SERVO_MIN);
}
void turnRightTowardsObstacle() {
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_TOWARDS_OBSTACLE_SERVO_MAX + 3);
}

void turnLeftTowardsObstacle() {
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_TOWARDS_OBSTACLE_SERVO_MIN);
}
void turnBack() {
  driveServo.writeMicroseconds(DRIVE_SERVO_NEUTRAL);
  steerServo.writeMicroseconds(STEER_SERVO_MIN);
}
void stopMotors() {
//  Serial.println("Stop Motors ");
  driveServo.writeMicroseconds(1500);
  steerServo.writeMicroseconds(1500);

  if (adjustNeutralValue) {
    DRIVE_SERVO_NEUTRAL = 1500;
    STEER_SERVO_NEUTRAL = 1501;
  }
  else {
    DRIVE_SERVO_NEUTRAL = 1500;
    STEER_SERVO_NEUTRAL = 1500;
  }
}
void moveForward() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MAX);
//  steerServo.writeMicroseconds(STEER_SERVO_MAX);

  driveServo.writeMicroseconds(DRIVE_SERVO_MAX);
  steerServo.writeMicroseconds(STEER_SERVO_NEUTRAL);
}
//void moveForward() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MAX);
//  steerServo.writeMicroseconds(STEER_SERVO_MAX);
//}
//
//void turnRight() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MIN + 5);
//  steerServo.writeMicroseconds(STEER_SERVO_MAX - 5);
//}
//
//void turnLeft() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MAX - 5);
//  steerServo.writeMicroseconds(STEER_SERVO_MIN + 5);
//}
//void turnRightTowardsObstacle() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MIN + 20);
//  steerServo.writeMicroseconds(STEER_SERVO_MAX - 20);
//}
//
//void turnLeftTowardsObstacle() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MAX - 20);
//  steerServo.writeMicroseconds(STEER_SERVO_MIN + 20);
//}
//void turnBack() {
//  driveServo.writeMicroseconds(DRIVE_SERVO_MAX);
//  steerServo.writeMicroseconds(STEER_SERVO_MIN);
//}
//void stopMotors() {
//  //Serial.println("Stop Motors ");
//  driveServo.writeMicroseconds(1500);
//  steerServo.writeMicroseconds(1500);
//
////  if (adjustNeutralValue) {
////    DRIVE_SERVO_NEUTRAL = 1500;
////    STEER_SERVO_NEUTRAL = 1501;
////  }
////  else {
////    DRIVE_SERVO_NEUTRAL = 1501;
////    STEER_SERVO_NEUTRAL = 1500;
////  }
//}

void monitorConnection() {
  if ((millis() - lastDataReceivedMillis) >= MAX_CONNECTION_IDLE_PERIOD) {
    lastDataReceivedMillis = millis();
    stopMotors();
  }
}

void blinkEye() {
  unsigned long blinkDelayMillis = 0;
  if (isEyeOpen) {
    blinkDelayMillis = 3000;
  }
  else {
    blinkDelayMillis = 250;
  }

  if ((millis() - lastBlinkedTimeMillis) >= blinkDelayMillis) {
    lastBlinkedTimeMillis = millis();
    if (isEyeOpen) {
      digitalWrite(EYE_PART1_OUTPUT_PIN, LOW);
      digitalWrite(EYE_PART2_OUTPUT_PIN, LOW);


      isEyeOpen = false;
    }
    else {
      digitalWrite(EYE_PART1_OUTPUT_PIN, HIGH);
      digitalWrite(EYE_PART2_OUTPUT_PIN, HIGH);

      isEyeOpen = true;
    }


  }
}

void calculateRemainingTurnTime() {
  if (turningRight || turningLeft) {
    if (!motorsStopped) {
      unsigned long turnElapsedTime = millis() - turnStartMillis;
      unsigned long remainingTime = simpleTurnDuration - turnElapsedTime;
      simpleTurnDuration = remainingTime;
//                Serial.print("elapsed: ");
//                Serial.print(turnElapsedTime);
//                Serial.print(", remainingTime");
//                Serial.print(remainingTime);
//                Serial.print(", simpleTurnDuration: ");
//                Serial.print(simpleTurnDuration);
//                Serial.print(", turnStartMillis: ");
//                Serial.println(turnStartMillis);
    }
    turnStartMillis = millis();
  }
  else if (turningBack) {
    if (!motorsStopped) {
      unsigned long turnElapsedTime = millis() - turnStartMillis;
      unsigned long remainingTime = turnBackDuration - turnElapsedTime;
      turnBackDuration = remainingTime;
//      Serial.print("elapsed: ");
//      Serial.print(turnElapsedTime);
//      Serial.print(", remainingTime");
//      Serial.print(remainingTime);
//      Serial.print(", turnBackDuration: ");
//      Serial.print(turnBackDuration);
//      Serial.print(", turnStartMillis: ");
//      Serial.println(turnStartMillis);
    }
    turnStartMillis = millis();
  }
}
void printTurnStatusCompleted() {
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& jsonObject = jsonBuffer.createObject();
  jsonObject["d_type"] = "TURN_STATUS";
  jsonObject["status"] = "COMPLETED";
  jsonObject["count"] = count++;
  jsonObject.printTo(Serial);
  Serial.println();
}
void printTurnStatusOngoing() {
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& jsonObject = jsonBuffer.createObject();
  jsonObject["d_type"] = "TURN_STATUS";
  jsonObject["status"] = "TURNING";
  jsonObject["count"] = count++;
  jsonObject.printTo(Serial);
  Serial.println();
}
